import math
import random
import matplotlib.pyplot as plt

def fit(i):
    return -.5 * (((1.1 * i)-5)**2) + abs(i*math.sin(i)) + 10

def roulette(pop,fit,remain):                      
    tot = 0
    for i in fit:
        if (i > 0): tot += i
    surviv = []
    for _ in range(remain):
        pointerVal = random.uniform(0,tot)
        part = 0
        for i in range(len(fit)):
            fitV = fit[i]
            if (fitV > 0): part += fitV
            if (part >= pointerVal):
                surviv.append(pop[i])
                break #Yeah, so sue me
    return surviv

def mate(pop,fit,newPop):
    tot = 0
    for i in fit:
        if (i > 0): tot += i
    kids = []
    for _ in range(newPop):
        parents = []
        for a in range(2):
            pointerVal = random.uniform(0,tot)
            part = 0
            for i in range(len(fit)):
                fitV = fit[i]
                if (fitV > 0): part += fitV
                if (part >= pointerVal):
                    parents.append(pop[i])
                    break
        kid = parents[0] + parents[1]
        kid = kid / 2
        kids.append(kid)
    return kids

def mutate(pop,mutC,mutV):
    for i in range(len(pop)):
        if (random.random() < mutC):
            diff = mutV * random.random()
            if (random.random() > .5): diff *= -1
            pop[i] = pop[i] + diff
    return pop

if __name__ == "__main__":
    topFits = []
    POP_SIZE = 50
    SURVIV_RATE = .8
    GEN_NUM = 700
    MUT_CHANCE = .4
    MUT_VAL = .1

    random.seed()
    population = []
    for _ in range(POP_SIZE):
            population.append(random.uniform(0,10))
    #print("Initial population is " + str(population))
    for gen in range(GEN_NUM):
        print("Generation " + str(gen + 1) + " starting")
        fitnesses = []
        for i in population:
            fitnesses.append(fit(i))
        population = roulette(population,fitnesses,int(POP_SIZE * SURVIV_RATE))
        fitnesses = []
        for i in population:
            fitnesses.append(fit(i))
        print("Current highest fitness is " + str(max(fitnesses)))
        topFits.append(max(fitnesses))
        population = mate(population,fitnesses,POP_SIZE)
        population = mutate(population,MUT_CHANCE,MUT_VAL)
        print("Generation " + str(gen + 1) + " Finished")

    #Find and display the top performer
    sPop = [x for _,x in sorted(zip(fitnesses,population))] #Python magic to sort the population
    sPop.reverse()
    sPop = sPop[0]
    fitnesses.sort()
    fitnesses.reverse()
    print("The optimal answer found was " + str(sPop) + ", with fitness value " + str(fitnesses[0]) + ".")
    plt.plot(range(GEN_NUM),topFits)
    plt.show()
